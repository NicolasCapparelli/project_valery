### THIS IS NOT THE MAIN PROJECT.
That can be found in valerymain

## Explanation for this directory

The directory babysteps/ is the remnants of me attempting to begin creating a speech recognition engine by myself. The actual There isn't much, because obviously I didn't get very far before realizing that A.) I was nowhere near skilled enough to do it and B.) It's not really something that should be done without a team.

I decided to not delete these as I like to look back and see failed attempts, even if they fail as early on as this one.