# Project Valery

My personal attempt at a personal assistant (without machine learning) written using the [speech_recoginition library](https://pypi.org/project/SpeechRecognition/).

## Current Features

- Keyword recognition with background listening

- Opening programs

- Searching the web

	- Using specific filters such as flights, books, etc...
	
	- Search specific websites (Youtube, Amazon)

- Windows (OS) specific tasks:

	- Computer power options (shutdown, sleep, etc...)

	- Opening files (Word, Powerpoint, Photoshop, etc..)

	- Pasting from clipboard

	- Close a window

	- Minimize all windows


- Writing emails

- Randomized voice responses (within context)

- Logitech Hardware listening indicators

	- When focus listening (not in background mode) the program lights up a pattern or a specific key on Logitech RGB keyboards to let the user know it is listening


# WIP
This repository is a Work In Progress
